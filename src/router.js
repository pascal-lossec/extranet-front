import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

export default new Router({
  mode: "history",
  routes: [
    {
      path: "/",
      alias: "/Connexion",
      name: "Connexion",
      component: () => import("./components/Connexion")
    },
    {
      path: "/Commandes",
      alias: "/Commandes",
      name: "Commandes",
      component: () => import("./components/Commandes")
    }
  ]
});